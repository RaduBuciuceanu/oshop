## OShop
It's the backend app for OShop system. It's written in .net core and 
offers a REST Api for OShop.Frontend app.

### Dependencies
* .net core

### All articles
This endpoint offers us the possibility to fetch all the created 
articles from server. It can be accessed here:
* http://localhost:5000/api/article/all using GET, HTTP method.

Result: 
```json
[
    {
      "id": "Random id here.",
      "name": "Random name here.",
      "description": "Random description here.",
      "price": 123
    }
]
```

### Create ball
This endpoint offers us the possibility to create a new ball on the 
server. It can be accessed here:
* http://localhost:5000/api/ball/create using POST, HTTP method.

Parameter:
```json
{
  "name": "Random name here.",
  "description": "Random description here.",
  "price": 123,
  "maximumPressure": 123,
  "diameter": 123
}
```

Result:
```json
{
  "id": "Random id here.",
  "name": "Random name here.",
  "description": "Random description here.",
  "price": 123,
  "maximumPressure": 123,
  "diameter": 123
}
```

### Create rollers
This endpoint offers us the possibility to create a new rollers on the 
server. It can be accessed here:
* http://localhost:5000/api/rollers/create using POST, HTTP method.

Parameter:
```json
{
  "name": "Random name here.",
  "description": "Random description here.",
  "price": 123,
  "wheelCount": 123,
  "size": 123
}
```

Result:
```json
{
  "id": "Random id here.",
  "name": "Random name here.",
  "description": "Random description here.",
  "price": 123,
  "wheelCount": 123,
  "size": 123
}
