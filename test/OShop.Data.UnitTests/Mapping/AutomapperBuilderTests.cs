using OShop.Data.Mapping;
using Shouldly;
using Xunit;

namespace OShop.Data.UnitTests.Mapping
{
    public class AutomapperBuilderTests
    {
        public AutomapperBuilderTests()
        {
            _instance = new AutomapperBuilder();
        }

        private readonly IAutomapperBuilder _instance;

        [Fact]
        public void Build_DoesNot_ReturnNull()
        {
            var actual = _instance.Build();

            actual.ShouldNotBeNull();
        }

        [Fact]
        public void WithMaps_Returns_Itself()
        {
            var actual = _instance.WithMaps();

            actual.ShouldBe(_instance);
        }
    }
}