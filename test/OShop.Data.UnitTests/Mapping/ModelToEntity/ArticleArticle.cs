using System.Reactive.Linq;
using FizzWare.NBuilder;
using Shouldly;
using Xunit;
using ArticleModel = OShop.Business.Models.Article;
using ArticleEntity = OShop.Data.Entities.Article;

namespace OShop.Data.UnitTests.Mapping.ModelToEntity
{
    public class ArticleArticle : AutomapperTests
    {
        private readonly ArticleModel _model = Builder<ArticleModel>.CreateNew().Build();

        private readonly ArticleEntity _actual;

        public ArticleArticle()
        {
            _actual = Instance.Map<ArticleModel, ArticleEntity>(_model).Wait();
        }

        [Fact]
        public void Map_Id_IsMapped()
        {
            _actual.Id.ShouldBe(_model.Id);
        }

        [Fact]
        public void Map_Name_IsMapped()
        {
            _actual.Id.ShouldBe(_model.Id);
        }

        [Fact]
        public void Map_Description_IsMapped()
        {
            _actual.Description.ShouldBe(_model.Description);
        }

        [Fact]
        public void Map_Price_IsMapped()
        {
            _actual.Price.ShouldBe(_model.Price);
        }
    }
}