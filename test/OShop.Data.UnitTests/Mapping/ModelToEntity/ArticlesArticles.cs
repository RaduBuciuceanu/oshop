using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using FizzWare.NBuilder;
using Shouldly;
using Xunit;
using ArticleModel = OShop.Business.Models.Article;
using ArticleEntity = OShop.Data.Entities.Article;

namespace OShop.Data.UnitTests.Mapping.ModelToEntity
{
    public class ArticlesArticles : AutomapperTests
    {
        private readonly IEnumerable<ArticleModel> _models = Builder<ArticleModel>.CreateListOfSize(1).Build();

        private readonly IEnumerable<ArticleEntity> _actual;

        public ArticlesArticles()
        {
            _actual = Instance.MapMany<ArticleModel, ArticleEntity>(_models).Wait();
        }

        [Fact]
        public void MapMany_Id_IsMapped()
        {
            _actual.ShouldAllBe(model => model.Id == _models.First().Id);
        }

        [Fact]
        public void MapMany_Name_IsMapped()
        {
            _actual.ShouldAllBe(model => model.Name == _models.First().Name);
        }

        [Fact]
        public void MapMany_Description_IsMapped()
        {
            _actual.ShouldAllBe(model => model.Description == _models.First().Description);
        }

        [Fact]
        public void MapMany_Price_IsMapped()
        {
            _actual.ShouldAllBe(model => model.Price == _models.First().Price);
        }
    }
}
