using System.Reactive.Linq;
using FizzWare.NBuilder;
using Shouldly;
using Xunit;
using RollersModel = OShop.Business.Models.Rollers;
using RollersEntity = OShop.Data.Entities.Rollers;

namespace OShop.Data.UnitTests.Mapping.ModelToEntity
{
    public class RollersRollers : AutomapperTests
    {
        private readonly RollersModel _model = Builder<RollersModel>.CreateNew().Build();

        private readonly RollersEntity _actual;

        public RollersRollers()
        {
            _actual = Instance.Map<RollersModel, RollersEntity>(_model).Wait();
        }

        [Fact]
        public void Map_Id_IsMapped()
        {
            _actual.Id.ShouldBe(_model.Id);
        }

        [Fact]
        public void Map_Name_IsMapped()
        {
            _actual.Name.ShouldBe(_model.Name);
        }

        [Fact]
        public void Map_Description_IsMapped()
        {
            _actual.Description.ShouldBe(_model.Description);
        }

        [Fact]
        public void Map_Price_IsMapped()
        {
            _actual.Price.ShouldBe(_model.Price);
        }

        [Fact]
        public void Map_WheelCount_IsMapped()
        {
            _actual.WheelCount.ShouldBe(_model.WheelCount);
        }

        [Fact]
        public void Map_Size_IsMapped()
        {
            _actual.Size.ShouldBe(_model.Size);
        }
    }
}
