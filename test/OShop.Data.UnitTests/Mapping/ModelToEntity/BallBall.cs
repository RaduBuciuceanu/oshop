using System.Reactive.Linq;
using FizzWare.NBuilder;
using Shouldly;
using Xunit;
using BallModel = OShop.Business.Models.Ball;
using BallEntity = OShop.Data.Entities.Ball;

namespace OShop.Data.UnitTests.Mapping.ModelToEntity
{
    public class BallBall : AutomapperTests
    {
        private readonly BallModel _model = Builder<BallModel>.CreateNew().Build();

        private readonly BallEntity _actual;

        public BallBall()
        {
            _actual = Instance.Map<BallModel, BallEntity>(_model).Wait();
        }

        [Fact]
        public void Map_Id_IsMapped()
        {
            _actual.Id.ShouldBe(_model.Id);
        }

        [Fact]
        public void Map_Name_IsMapped()
        {
            _actual.Name.ShouldBe(_model.Name);
        }

        [Fact]
        public void Map_Description_IsMapped()
        {
            _actual.Description.ShouldBe(_model.Description);
        }

        [Fact]
        public void Map_Price_IsMapped()
        {
            _actual.Price.ShouldBe(_model.Price);
        }

        [Fact]
        public void Map_MaximumPressure_IsMapped()
        {
            _actual.MaximumPressure.ShouldBe(_model.MaximumPressure);
        }

        [Fact]
        public void Map_Diameter_IsMapped()
        {
            _actual.Diameter.ShouldBe(_model.Diameter);
        }
    }
}