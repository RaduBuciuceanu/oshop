using System.Reactive.Linq;
using FizzWare.NBuilder;
using Shouldly;
using Xunit;
using RollersModel = OShop.Business.Models.Rollers;
using RollersEntity = OShop.Data.Entities.Rollers;

namespace OShop.Data.UnitTests.Mapping.EntityToMode
{
    public class RollersRollers : AutomapperTests
    {
        private readonly RollersEntity _entity = Builder<RollersEntity>.CreateNew().Build();

        private readonly RollersModel _actual;

        public RollersRollers()
        {
            _actual = Instance.Map<RollersEntity, RollersModel>(_entity).Wait();
        }

        [Fact]
        public void Map_Id_IsMapped()
        {
            _actual.Id.ShouldBe(_entity.Id);
        }

        [Fact]
        public void Map_Name_IsMapped()
        {
            _actual.Name.ShouldBe(_entity.Name);
        }

        [Fact]
        public void Map_Description_IsMapped()
        {
            _actual.Description.ShouldBe(_entity.Description);
        }

        [Fact]
        public void Map_Price_IsMapped()
        {
            _actual.Price.ShouldBe(_entity.Price);
        }

        [Fact]
        public void Map_WheelCount_IsMapped()
        {
            _actual.WheelCount.ShouldBe(_entity.WheelCount);
        }

        [Fact]
        public void Map_Size_IsMapped()
        {
            _actual.Size.ShouldBe(_entity.Size);
        }
    }
}