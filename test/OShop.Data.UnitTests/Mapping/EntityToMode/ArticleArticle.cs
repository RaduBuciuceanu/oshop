using System.Reactive.Linq;
using FizzWare.NBuilder;
using Shouldly;
using Xunit;
using ArticleModel = OShop.Business.Models.Article;
using ArticleEntity = OShop.Data.Entities.Article;

namespace OShop.Data.UnitTests.Mapping.EntityToMode
{
    public class ArticleArticle : AutomapperTests
    {
        private readonly ArticleEntity _entity = Builder<ArticleEntity>.CreateNew().Build();

        private readonly ArticleModel _actual;

        public ArticleArticle()
        {
            _actual = Instance.Map<ArticleEntity, ArticleModel>(_entity).Wait();
        }

        [Fact]
        public void Map_Id_IsMapped()
        {
            _actual.Id.ShouldBe(_entity.Id);
        }

        [Fact]
        public void Map_Name_IsMapped()
        {
            _actual.Id.ShouldBe(_entity.Id);
        }

        [Fact]
        public void Map_Description_IsMapped()
        {
            _actual.Description.ShouldBe(_entity.Description);
        }

        [Fact]
        public void Map_Price_IsMapped()
        {
            _actual.Price.ShouldBe(_entity.Price);
        }
    }
}