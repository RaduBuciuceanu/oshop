using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using FizzWare.NBuilder;
using Shouldly;
using Xunit;
using ArticleModel = OShop.Business.Models.Article;
using ArticleEntity = OShop.Data.Entities.Article;

namespace OShop.Data.UnitTests.Mapping.EntityToMode
{
    public class ArticlesArticles : AutomapperTests
    {
        private readonly IEnumerable<ArticleEntity> _entities = Builder<ArticleEntity>.CreateListOfSize(1).Build();

        private readonly IEnumerable<ArticleModel> _actual;

        public ArticlesArticles()
        {
            _actual = Instance.MapMany<ArticleEntity, ArticleModel>(_entities).Wait();
        }

        [Fact]
        public void MapMany_Id_IsMapped()
        {
            _actual.ShouldAllBe(model => model.Id == _entities.First().Id);
        }

        [Fact]
        public void MapMany_Name_IsMapped()
        {
            _actual.ShouldAllBe(model => model.Name == _entities.First().Name);
        }

        [Fact]
        public void MapMany_Description_IsMapped()
        {
            _actual.ShouldAllBe(model => model.Description == _entities.First().Description);
        }

        [Fact]
        public void MapMany_Price_IsMapped()
        {
            _actual.ShouldAllBe(model => model.Price == _entities.First().Price);
        }
    }
}
