using System.Reactive.Linq;
using FizzWare.NBuilder;
using Shouldly;
using Xunit;
using BallModel = OShop.Business.Models.Ball;
using BallEntity = OShop.Data.Entities.Ball;

namespace OShop.Data.UnitTests.Mapping.EntityToMode
{
    public class BallBall : AutomapperTests
    {
        private readonly BallEntity _entity = Builder<BallEntity>.CreateNew().Build();

        private readonly BallModel _actual;

        public BallBall()
        {
            _actual = Instance.Map<BallEntity, BallModel>(_entity).Wait();
        }

        [Fact]
        public void Map_Id_IsMapped()
        {
            _actual.Id.ShouldBe(_entity.Id);
        }

        [Fact]
        public void Map_Name_IsMapped()
        {
            _actual.Name.ShouldBe(_entity.Name);
        }

        [Fact]
        public void Map_Description_IsMapped()
        {
            _actual.Description.ShouldBe(_entity.Description);
        }

        [Fact]
        public void Map_Price_IsMapped()
        {
            _actual.Price.ShouldBe(_entity.Price);
        }

        [Fact]
        public void Map_MaximumPressure_IsMapped()
        {
            _actual.MaximumPressure.ShouldBe(_entity.MaximumPressure);
        }

        [Fact]
        public void Map_Diameter_IsMapped()
        {
            _actual.Diameter.ShouldBe(_entity.Diameter);
        }
    }
}