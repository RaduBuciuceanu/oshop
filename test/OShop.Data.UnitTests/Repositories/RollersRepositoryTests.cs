using System.Reactive.Linq;
using Moq.AutoMock;
using OShop.Business.Repositories;
using OShop.Data.Mapping;
using OShop.Data.Repositories;
using Xunit;
using RollersModel = OShop.Business.Models.Rollers;
using RollersEntity = OShop.Data.Entities.Rollers;

namespace OShop.Data.UnitTests.Repositories
{
    public class RollersRepositoryTests
    {
        private readonly AutoMocker _mocker = new AutoMocker();
        private readonly RollersModel _model = new RollersModel();
        private readonly RollersEntity _entity = new RollersEntity();

        private readonly IRollersRepository _instance;

        public RollersRepositoryTests()
        {
            SetupAutomapper();
            SetupStorage();
            _instance = _mocker.CreateInstance<RollersRepository>();
        }

        [Fact]
        public void Insert_InvokesMapFromAutomapper_ForModelToEntityMapping()
        {
            _instance.Insert(_model).Wait();

            _mocker.Verify<IAutomapper>(mapper => mapper.Map<RollersModel, RollersEntity>(_model));
        }

        [Fact]
        public void Insert_InvokesInsert_FromStorage()
        {
            _instance.Insert(_model).Wait();

            _mocker.Verify<IStorage>(storage => storage.Insert(_entity));
        }

        [Fact]
        public void Insert_InvokesMapFromAutomapper_ForEntityToModelMapping()
        {
            _instance.Insert(_model).Wait();

            _mocker.Verify<IAutomapper>(mapper => mapper.Map<RollersEntity, RollersModel>(_entity));
        }

        private void SetupAutomapper()
        {
            _mocker.GetMock<IAutomapper>()
                .Setup(mapper => mapper.Map<RollersModel, RollersEntity>(_model))
                .Returns(Observable.Return(_entity));

            _mocker.GetMock<IAutomapper>()
                .Setup(mapper => mapper.Map<RollersEntity, RollersModel>(_entity))
                .Returns(Observable.Return(_model));
        }

        private void SetupStorage()
        {
            _mocker.GetMock<IStorage>()
                .Setup(storage => storage.Insert(_entity))
                .Returns(Observable.Return(_entity));
        }
    }
}
