using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Moq.AutoMock;
using OShop.Business.Repositories;
using OShop.Data.Mapping;
using OShop.Data.Repositories;
using Shouldly;
using Xunit;
using ArticleModel = OShop.Business.Models.Article;
using ArticleEntity = OShop.Data.Entities.Article;

namespace OShop.Data.UnitTests.Repositories
{
    public class ArticleRepositoryTests
    {
        private readonly AutoMocker _mocker = new AutoMocker();
        private readonly IQueryable<ArticleEntity> _storageResult = Enumerable.Empty<ArticleEntity>().AsQueryable();
        private readonly IEnumerable<ArticleModel> _mapperResult = Enumerable.Empty<ArticleModel>();

        private readonly IArticleRepository _instance;

        public ArticleRepositoryTests()
        {
            SetupStorage();
            SetupAutomapper();
            _instance = _mocker.CreateInstance<ArticleRepository>();
        }

        [Fact]
        public void GetAll_InvokesGet_FromStorage()
        {
            _instance.GetAll().Wait();

            _mocker.Verify<IStorage>(storage => storage.Get<ArticleEntity>());
        }

        [Fact]
        public void GetAll_InvokesMap_FromAutomapper()
        {
            _instance.GetAll().Wait();

            _mocker.Verify<IAutomapper>(mapper => mapper.MapMany<ArticleEntity, ArticleModel>(_storageResult));
        }

        [Fact]
        public void GetAll_Returns_MapperResult()
        {
            IEnumerable<ArticleModel> actual = _instance.GetAll().Wait();

            actual.ShouldBe(_mapperResult);
        }

        private void SetupStorage()
        {
            _mocker.GetMock<IStorage>()
                .Setup(storage => storage.Get<ArticleEntity>())
                .Returns(Observable.Return(_storageResult));
        }

        private void SetupAutomapper()
        {
            _mocker.GetMock<IAutomapper>()
                .Setup(mapper => mapper.MapMany<ArticleEntity, ArticleModel>(_storageResult))
                .Returns(Observable.Return(_mapperResult));
        }
    }
}