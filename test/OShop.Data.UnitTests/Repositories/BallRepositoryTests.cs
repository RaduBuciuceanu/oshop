using System.Reactive.Linq;
using Moq.AutoMock;
using OShop.Business.Repositories;
using OShop.Data.Mapping;
using OShop.Data.Repositories;
using Shouldly;
using Xunit;
using BallModel = OShop.Business.Models.Ball;
using BallEntity = OShop.Data.Entities.Ball;

namespace OShop.Data.UnitTests.Repositories
{
    public class BallRepositoryTests
    {
        private readonly AutoMocker _mocker = new AutoMocker();
        private readonly BallModel _model = new BallModel();
        private readonly BallEntity _entity = new BallEntity();

        private readonly IBallRepository _instance;

        public BallRepositoryTests()
        {
            SetupAutomapper();
            SetupStorage();
            _instance = _mocker.CreateInstance<BallRepository>();
        }

        [Fact]
        public void Insert_InvokesMapFromAutomapper_ForModelToEntityMapping()
        {
            _instance.Insert(_model).Wait();

            _mocker.Verify<IAutomapper>(mapper => mapper.Map<BallModel, BallEntity>(_model));
        }

        [Fact]
        public void Insert_InvokesInsert_FromStorage()
        {
            _instance.Insert(_model).Wait();

            _mocker.Verify<IStorage>(storage => storage.Insert(_entity));
        }

        [Fact]
        public void Insert_InvokesMapFromAutomapper_ForEntityToModelMapping()
        {
            _instance.Insert(_model).Wait();

            _mocker.Verify<IAutomapper>(mapper => mapper.Map<BallEntity, BallModel>((_entity)));
        }

        [Fact]
        public void Insert_ReturnsResult_FromAutomapper()
        {
            BallModel actual = _instance.Insert(_model).Wait();

            actual.ShouldBe(_model);
        }

        private void SetupAutomapper()
        {
            _mocker.GetMock<IAutomapper>()
                .Setup(mapper => mapper.Map<BallModel, BallEntity>(_model))
                .Returns(Observable.Return(_entity));

            _mocker.GetMock<IAutomapper>()
                .Setup(mapper => mapper.Map<BallEntity, BallModel>(_entity))
                .Returns(Observable.Return(_model));
        }

        private void SetupStorage()
        {
            _mocker.GetMock<IStorage>()
                .Setup(storage => storage.Insert(_entity))
                .Returns(Observable.Return(_entity));
        }
    }
}
