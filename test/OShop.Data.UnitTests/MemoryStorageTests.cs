using System;
using System.Linq;
using System.Reactive.Linq;
using Moq;
using OShop.Data.Entities;
using Shouldly;
using Xunit;

namespace OShop.Data.UnitTests
{
    public class MemoryStorageTests
    {
        private readonly Entity _storedEntity = Mock.Of<Entity>();
        private readonly Entity _notStoredEntity = Mock.Of<Entity>();

        private readonly IStorage _instance;

        public MemoryStorageTests()
        {
            _storedEntity.Id = "Random id here.";
            _notStoredEntity.Id = "Other random id here.";
            _instance = new MemoryStorage();
            _instance.Insert(_storedEntity).Wait();
        }

        [Fact]
        public void Insert_Throws_WhenIdAlreadyExists()
        {
            Action act = () => _instance.Insert(_storedEntity).Wait();

            act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Insert_Stores_Entity()
        {
            _instance.Insert(_notStoredEntity).Wait();

            _instance.Get<Entity>().Wait().ShouldContain(existent => existent.Id == _notStoredEntity.Id);
        }

        [Fact]
        public void Insert_Returns_StoredEntity()
        {
            Entity actual = _instance.Insert(_notStoredEntity).Wait();

            actual.ShouldBe(_notStoredEntity);
        }

        [Fact]
        public void Update_Throws_WhenIdDoesNotExist()
        {
            Action act = () => _instance.Update(_notStoredEntity);

            act.ShouldThrow<Exception>();
        }

        [Fact]
        public void Update_UpdatesTheEntity_WhenIdExists()
        {
            _notStoredEntity.Id = _storedEntity.Id;

            _instance.Update(_notStoredEntity).Wait();

            _instance.Get<Entity>().Wait().Single().ShouldBe(_notStoredEntity);
        }

        [Fact]
        public void Update_Returns_ChangedEntity()
        {
            _notStoredEntity.Id = _storedEntity.Id;

            Entity actual = _instance.Update(_notStoredEntity).Wait();

            actual.ShouldBe(_notStoredEntity);
        }

        [Fact]
        public void Delete_Throws_WhenIdDoesNotExist()
        {
            Action act = () => _instance.Delete(_notStoredEntity).Wait();

            act.ShouldThrow<Exception>();
        }

        [Fact]
        public void Delete_Deletes_Entity()
        {
            _instance.Delete(_storedEntity).Wait();

            _instance.Get<Entity>().Wait().ShouldBeEmpty();
        }

        [Fact]
        public void Delete_Returns_DeletedEntity()
        {
            Entity actual = _instance.Delete(_storedEntity).Wait();

            actual.ShouldBe(_storedEntity);
        }

        [Fact]
        public void Get_Returns_AllEntities()
        {
            _instance.Insert(_notStoredEntity).Wait();

            IQueryable<Entity> actual = _instance.Get<Entity>().Wait();

            actual.Count().ShouldBe(2);
        }
    }
}