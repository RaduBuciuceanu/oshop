using Microsoft.Extensions.DependencyInjection;
using OShop.Business.Commands;
using OShop.Business.Commands.Articles;
using OShop.Business.Commands.Balls;
using OShop.Business.Commands.Rollers;
using OShop.Business.Repositories;
using OShop.Data;
using OShop.Data.Mapping;
using OShop.Data.Repositories;
using OShop.Presentation.Startup;
using OShop.Validations.Balls;
using OShop.Validations.Rollers;
using Shouldly;
using Xunit;
using MemoryStorage = OShop.Data.MemoryStorage;

namespace OShop.Presentation.UnitTests.Startup
{
    public class StartupMockTests
    {
        private readonly IServiceCollection _collection = new ServiceCollection();

        public StartupMockTests()
        {
            StartupMock.ConfigureServices(_collection);
        }

        [Fact]
        public void ConfigureServices_IStorage_IsRegistered()
        {
            AssertServiceExists<IStorage, MemoryStorage>();
        }

        [Fact]
        public void ConfigureServices_IAutomapper_IsRegistered()
        {
            AssertServiceExists<IAutomapper>();
        }

        [Fact]
        public void ConfigureServices_IArticleRepository_IsRegistered()
        {
            AssertServiceExists<IArticleRepository, ArticleRepository>();
        }

        [Fact]
        public void ConfigureServices_IBallRepository_IsRegistered()
        {
            AssertServiceExists<IBallRepository, BallRepository>();
        }

        [Fact]
        public void ConfigureServices_IRollersRepository_IsRegistered()
        {
            AssertServiceExists<IRollersRepository, RollersRepository>();
        }

        [Fact]
        public void ConfigureServices_ISetIdentifier_IsRegistered()
        {
            AssertServiceExists<ISetIdentifier, SetIdentifier>();
        }

        [Fact]
        public void ConfigureServices_IValidateBall_IsRegistered()
        {
            AssertServiceExists<IValidateBall, ValidateBall>();
        }

        [Fact]
        public void ConfigureServices_ICreateBall_IsRegistered()
        {
            AssertServiceExists<ICreateBall, CreateBall>();
        }

        [Fact]
        public void ConfigureServices_IValidateRollers_IsRegistered()
        {
            AssertServiceExists<IValidateRollers, ValidateRollers>();
        }

        [Fact]
        public void ConfigureServices_ICreateRollers_IsRegistered()
        {
            AssertServiceExists<ICreateRollers, CreateRollers>();
        }

        [Fact]
        public void ConfigureServices_IGetAllArticles_IsRegistered()
        {
            AssertServiceExists<IGetAllArticles, GetAllArticles>();
        }

        private void AssertServiceExists<TService, TImplementation>()
        {
            _collection.ShouldContain(service => service.ServiceType == typeof(TService) &&
                                                 service.ImplementationType == typeof(TImplementation));
        }

        private void AssertServiceExists<TService>()
        {
            _collection.ShouldContain(service => service.ServiceType == typeof(TService));
        }
    }
}