using System.Reactive.Linq;
using Moq.AutoMock;
using OShop.Business.Commands.Balls;
using OShop.Business.Commands.Rollers;
using OShop.Business.Models;
using OShop.Presentation.Controllers;
using Shouldly;
using Xunit;

namespace OShop.Presentation.UnitTests.Controllers
{
    public class RollersControllerTests
    {
        private readonly AutoMocker _mocker = new AutoMocker();
        private readonly Rollers _model = new Rollers();

        private readonly RollersController _instance;

        public RollersControllerTests()
        {
            SetupCreateRollers();
            _instance = _mocker.CreateInstance<RollersController>();
        }

        [Fact]
        public void Create_InvokesExecute_FromCreateRollers()
        {
            _instance.Create(_model);

            _mocker.Verify<ICreateRollers>(create => create.Execute(_model));
        }

        [Fact]
        public void Create_ReturnsResult_FromCreateRollers()
        {
            Rollers actual = _instance.Create(_model);

            actual.ShouldBe(_model);
        }

        private void SetupCreateRollers()
        {
            _mocker.GetMock<ICreateRollers>()
                .Setup(create => create.Execute(_model))
                .Returns(Observable.Return(_model));
        }
    }
}