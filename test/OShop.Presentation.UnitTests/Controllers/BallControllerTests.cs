using System.Reactive.Linq;
using Moq.AutoMock;
using OShop.Business.Commands.Balls;
using OShop.Business.Models;
using OShop.Presentation.Controllers;
using Shouldly;
using Xunit;

namespace OShop.Presentation.UnitTests.Controllers
{
    public class BallControllerTests
    {
        private readonly AutoMocker _mocker = new AutoMocker();
        private readonly Ball _model = new Ball();

        private readonly BallController _instance;

        public BallControllerTests()
        {
            SetupCreateBall();
            _instance = _mocker.CreateInstance<BallController>();
        }

        [Fact]
        public void Create_InvokesExecute_FromCreateBall()
        {
            _instance.Create(_model);

            _mocker.Verify<ICreateBall>(create => create.Execute(_model));
        }

        [Fact]
        public void Create_ReturnsResult_FromCreateBall()
        {
            Ball actual = _instance.Create(_model);

            actual.ShouldBe(_model);
        }

        private void SetupCreateBall()
        {
            _mocker.GetMock<ICreateBall>()
                .Setup(create => create.Execute(_model))
                .Returns(Observable.Return(_model));
        }
    }
}
