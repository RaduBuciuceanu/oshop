using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Moq;
using Moq.AutoMock;
using OShop.Business.Commands.Articles;
using OShop.Business.Models;
using OShop.Presentation.Controllers;
using Shouldly;
using Xunit;

namespace OShop.Presentation.UnitTests.Controllers
{
    public class ArticleControllerTests
    {
        private readonly AutoMocker _mocker = new AutoMocker();
        private readonly IEnumerable<Article> _articles = Enumerable.Empty<Article>();

        private readonly ArticleController _instance;

        public ArticleControllerTests()
        {
            SetupGetAllArticles();
            _instance = _mocker.CreateInstance<ArticleController>();
        }

        [Fact]
        public void All_InvokesExecute_FromGetAllArticles()
        {
            _instance.All();

            _mocker.Verify<IGetAllArticles>(get => get.Execute(), Times.Once);
        }

        [Fact]
        public void All_ReturnsResult_FromGetAllArticles()
        {
            IEnumerable<Article> actual = _instance.All();

            actual.ShouldBe(_articles);
        }

        private void SetupGetAllArticles()
        {
            _mocker.GetMock<IGetAllArticles>()
                .Setup(get => get.Execute())
                .Returns(Observable.Return(_articles));
        }
    }
}
