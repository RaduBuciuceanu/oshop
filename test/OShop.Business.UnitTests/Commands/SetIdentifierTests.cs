using System.Reactive.Linq;
using OShop.Business.Commands;
using OShop.Business.Models;
using Shouldly;
using Xunit;

namespace OShop.Business.UnitTests.Commands
{
    public class SetIdentifierTests
    {
        private readonly Model _model = new Model();

        private readonly ISetIdentifier _instance;

        public SetIdentifierTests()
        {
            _instance = new SetIdentifier();
        }

        [Fact]
        public void Execute_Sets_Id()
        {
            _instance.Execute(_model).Wait();

            _model.Id.ShouldNotBeNullOrWhiteSpace();
        }

        [Fact]
        public void Execute_Sets_DifferentIds()
        {
            Model first = _instance.Execute(new Model()).Wait();

            Model actual = _instance.Execute(_model).Wait();

            actual.Id.ShouldNotBe(first.Id);
        }

        [Fact]
        public void Execute_Returns_Model()
        {
            Model actual = _instance.Execute(_model).Wait();

            actual.ShouldBe(_model);
        }
    }
}
