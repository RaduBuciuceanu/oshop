using System;
using System.Reactive;
using System.Reactive.Linq;
using Moq;
using Moq.AutoMock;
using OShop.Business.Commands;
using OShop.Business.Commands.Balls;
using OShop.Business.Repositories;
using Shouldly;
using Xunit;
using BallModel = OShop.Business.Models.Ball;

namespace OShop.Business.UnitTests.Commands.Balls
{
    public class CreateBallTests
    {
        private readonly AutoMocker _mocker = new AutoMocker();
        private readonly BallModel _model = new BallModel();

        private readonly ICreateBall _instance;

        public CreateBallTests()
        {
            SetupValidateBall();
            SetupSetIdentifier();
            SetupBallRepository();
            _instance = _mocker.CreateInstance<CreateBall>();
        }

        [Fact]
        public void Execute_InvokesExecute_FromValidateBall()
        {
            _instance.Execute(_model).Wait();

            _mocker.Verify<IValidateBall>(validate => validate.Execute(_model));
        }

        [Fact]
        public void Execute_Throws_WhenValidateBallThrows()
        {
            SetupThrowableValidateBall();

            Action act = () => _instance.Execute(_model).Wait();

            act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_InvokesExecute_FromSetIdentifier()
        {
            _instance.Execute(_model).Wait();

            _mocker.Verify<ISetIdentifier>(set => set.Execute(_model), Times.Once);
        }

        [Fact]
        public void Execute_InvokesInsertFromRepository_WhenValidateDoesNotThrow()
        {
            _instance.Execute(_model).Wait();

            _mocker.Verify<IBallRepository>(repository => repository.Insert(_model));
        }

        [Fact]
        public void Execute_DoesNotInvokeInsertFromRepository_WhenValidateThrows()
        {
            SetupThrowableValidateBall();

            SafeAct();

            _mocker.Verify<IBallRepository>(repository => repository.Insert(_model), Times.Never);
        }

        [Fact]
        public void Execute_ReturnsResult_FromRepository()
        {
            BallModel actual = _instance.Execute(_model).Wait();

            actual.ShouldBe(_model);
        }

        private void SetupSetIdentifier()
        {
            _mocker.GetMock<ISetIdentifier>()
                .Setup(set => set.Execute(_model))
                .Returns(Observable.Return(_model));
        }

        private void SetupValidateBall()
        {
            _mocker.GetMock<IValidateBall>()
                .Setup(validate => validate.Execute(_model))
                .Returns(Observable.Return(_model));
        }

        private void SetupThrowableValidateBall()
        {
            _mocker.GetMock<IValidateBall>()
                .Setup(validate => validate.Execute(_model))
                .Throws<ArgumentException>();
        }

        private void SetupBallRepository()
        {
            _mocker.GetMock<IBallRepository>()
                .Setup(repository => repository.Insert(_model))
                .Returns(Observable.Return(_model));
        }

        private void SafeAct()
        {
            Observable.Return(_model)
                .Select(_instance.Execute)
                .Switch()
                .Catch(Observable.Return(_model))
                .Wait();
        }
    }
}
