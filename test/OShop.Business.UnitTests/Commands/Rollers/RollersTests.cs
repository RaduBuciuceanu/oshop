using System;
using System.Reactive.Linq;
using Moq;
using Moq.AutoMock;
using OShop.Business.Commands;
using OShop.Business.Commands.Rollers;
using OShop.Business.Repositories;
using Shouldly;
using Xunit;
using RollersModel = OShop.Business.Models.Rollers;

namespace OShop.Business.UnitTests.Commands.Rollers
{
    public class RollersTests
    {
        private readonly AutoMocker _mocker = new AutoMocker();
        private readonly RollersModel _model = new RollersModel();

        private readonly ICreateRollers _instance;

        public RollersTests()
        {
            SetupValidateRollers();
            SetupSetIdentifier();
            SetupRollersRepository();
            _instance = _mocker.CreateInstance<CreateRollers>();
        }

        [Fact]
        public void Execute_InvokesExecute_FromValidateRollers()
        {
            _instance.Execute(_model).Wait();

            _mocker.Verify<IValidateRollers>(validate => validate.Execute(_model));
        }

        [Fact]
        public void Execute_Throws_WhenValidateRollersThrows()
        {
            SetupThrowableValidateRollers();

            Action act = () => _instance.Execute(_model).Wait();

            act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_InvokesExecute_FromSetIdentifier()
        {
            _instance.Execute(_model).Wait();

            _mocker.Verify<ISetIdentifier>(set => set.Execute(_model), Times.Once);
        }

        [Fact]
        public void Execute_InvokesInsert_FromRepository()
        {
            _instance.Execute(_model).Wait();

            _mocker.Verify<IRollersRepository>(repository => repository.Insert(_model));
        }

        [Fact]
        public void Execute_DoesNotInvokeInsertFromRepository_WhenValidateRollersThrows()
        {
            SetupThrowableValidateRollers();

            SafeAct();

            _mocker.Verify<IRollersRepository>(repository => repository.Insert(_model), Times.Never);
        }

        [Fact]
        public void Execute_ReturnsResult_FromRepository()
        {
            RollersModel actual = _instance.Execute(_model).Wait();

            actual.ShouldBe(_model);
        }

        private void SetupSetIdentifier()
        {
            _mocker.GetMock<ISetIdentifier>()
                .Setup(set => set.Execute(_model))
                .Returns(Observable.Return(_model));
        }

        private void SetupValidateRollers()
        {
            _mocker.GetMock<IValidateRollers>()
                .Setup(validate => validate.Execute(_model))
                .Returns(Observable.Return(_model));
        }

        private void SetupThrowableValidateRollers()
        {
            _mocker.GetMock<IValidateRollers>()
                .Setup(validate => validate.Execute(_model))
                .Throws<ArgumentException>();
        }

        private void SetupRollersRepository()
        {
            _mocker.GetMock<IRollersRepository>()
                .Setup(repository => repository.Insert(_model))
                .Returns(Observable.Return(_model));
        }

        private void SafeAct()
        {
            Observable.Return(_model)
                .Select(_instance.Execute)
                .Switch()
                .Catch(Observable.Return(_model))
                .Wait();
        }
    }
}
