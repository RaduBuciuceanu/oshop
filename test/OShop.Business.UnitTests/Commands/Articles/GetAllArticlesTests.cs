using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Moq;
using Moq.AutoMock;
using OShop.Business.Commands.Articles;
using OShop.Business.Models;
using OShop.Business.Repositories;
using Shouldly;
using Xunit;

namespace OShop.Business.UnitTests.Commands.Articles
{
    public class GetAllArticlesTests
    {
        private readonly AutoMocker _mocker = new AutoMocker();
        private readonly IEnumerable<Article> _articles = Enumerable.Empty<Article>();

        private readonly IGetAllArticles _instance;

        public GetAllArticlesTests()
        {
            SetupArticleRepository();
            _instance = _mocker.CreateInstance<GetAllArticles>();
        }

        [Fact]
        public void Execute_InvokesGetAll_FromRepository()
        {
            _instance.Execute().Wait();

            _mocker.Verify<IArticleRepository>(repository => repository.GetAll(), Times.Once);
        }

        [Fact]
        public void Execute_ReturnsResult_FromRepository()
        {
            IEnumerable<Article> actual = _instance.Execute().Wait();

            actual.ShouldBe(_articles);
        }

        private void SetupArticleRepository()
        {
            _mocker.GetMock<IArticleRepository>()
                .Setup(repository => repository.GetAll())
                .Returns(Observable.Return(_articles));
        }
    }
}