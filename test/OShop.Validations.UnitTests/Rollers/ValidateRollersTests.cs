using System;
using System.Reactive.Linq;
using FizzWare.NBuilder;
using OShop.Validations.Rollers;
using Shouldly;
using Xunit;
using RollersModel = OShop.Business.Models.Rollers;

namespace OShop.Validations.UnitTests.Rollers
{
    public class ValidateRollersTests
    {
        private readonly RollersModel _model = Builder<RollersModel>.CreateNew().Build();

        private readonly Action _act;

        public ValidateRollersTests()
        {
            _act = () => new ValidateRollers().Execute(_model).Wait();
        }

        [Fact]
        public void Execute_Throws_WhenIdIsNotNull()
        {
            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenNameIsNull()
        {
            _model.Name = null;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenNameIsEmpty()
        {
            _model.Name = string.Empty;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenDescriptionIsNull()
        {
            _model.Description = null;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenDescriptionIsEmpty()
        {
            _model.Description = string.Empty;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenPriceIsNull()
        {
            _model.Price = null;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenPriceIsEmpty()
        {
            _model.Price = string.Empty;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenWheelCountIsZero()
        {
            _model.WheelCount = 0;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenSizeIsNull()
        {
            _model.Size = null;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenSizeIsEmpty()
        {
            _model.Size = string.Empty;

            _act.ShouldThrow<ArgumentException>();
        }
    }
}