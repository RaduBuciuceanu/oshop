using System;
using System.Reactive.Linq;
using FizzWare.NBuilder;
using OShop.Business.Models;
using OShop.Validations.Balls;
using Shouldly;
using Xunit;

namespace OShop.Validations.UnitTests.Balls
{
    public class ValidateBallTests
    {
        private readonly Ball _model = Builder<Ball>.CreateNew().Build();

        private readonly Action _act;

        public ValidateBallTests()
        {
            _act = () => new ValidateBall().Execute(_model).Wait();
        }

        [Fact]
        public void Execute_Throws_WhenIdIsNotNull()
        {
            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenNameIsNull()
        {
            _model.Name = null;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenNameIsEmpty()
        {
            _model.Name = string.Empty;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenDescriptionIsNull()
        {
            _model.Description = null;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenDescriptionIsEmpty()
        {
            _model.Description = string.Empty;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenPriceIsNull()
        {
            _model.Price = null;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenPriceIsEmpty()
        {
            _model.Price = string.Empty;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenMaximumPressureIsNull()
        {
            _model.MaximumPressure = null;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenMaximumPressureIsEmpty()
        {
            _model.MaximumPressure = string.Empty;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenDiameterIsNull()
        {
            _model.Diameter = null;

            _act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Execute_Throws_WhenDiameterIsEmpty()
        {
            _model.Diameter = string.Empty;

            _act.ShouldThrow<ArgumentException>();
        }
    }
}
