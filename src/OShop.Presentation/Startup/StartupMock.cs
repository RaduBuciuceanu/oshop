using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using OShop.Presentation.Startup.Ioc;

namespace OShop.Presentation.Startup
{
    public class StartupMock
    {
        public static void ConfigureServices(IServiceCollection collection)
        {
            collection.AddMvc();

            collection.AddCors(options => options.AddPolicy("*", builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
            ));

            new MemoryStorage().Execute(collection);
            new DataAutomapper().Execute(collection);
            new BusinessRepositories().Execute(collection);
            new BusinessCommands().Execute(collection);
        }

        public static void Configure(IApplicationBuilder builder)
        {
            builder.UseMvc();
            builder.UseCors();
        }
    }
}
