using Microsoft.Extensions.DependencyInjection;
using OShop.Business.Commands;
using OShop.Business.Commands.Articles;
using OShop.Business.Commands.Balls;
using OShop.Business.Commands.Rollers;
using OShop.Validations.Balls;
using OShop.Validations.Rollers;

namespace OShop.Presentation.Startup.Ioc
{
    internal class BusinessCommands : ISetup
    {
        public void Execute(IServiceCollection collection)
        {
            collection.AddTransient<ISetIdentifier, SetIdentifier>();
            collection.AddTransient<IValidateBall, ValidateBall>();
            collection.AddTransient<ICreateBall, CreateBall>();
            collection.AddTransient<IValidateRollers, ValidateRollers>();
            collection.AddTransient<ICreateRollers, CreateRollers>();
            collection.AddTransient<IGetAllArticles, GetAllArticles>();
        }
    }
}