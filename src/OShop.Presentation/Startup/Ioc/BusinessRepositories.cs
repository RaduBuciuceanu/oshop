using Microsoft.Extensions.DependencyInjection;
using OShop.Business.Repositories;
using OShop.Data.Repositories;

namespace OShop.Presentation.Startup.Ioc
{
    internal class BusinessRepositories : ISetup
    {
        public void Execute(IServiceCollection collection)
        {
            collection.AddTransient<IArticleRepository, ArticleRepository>();
            collection.AddTransient<IBallRepository, BallRepository>();
            collection.AddTransient<IRollersRepository, RollersRepository>();
        }
    }
}
