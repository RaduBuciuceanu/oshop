using Microsoft.Extensions.DependencyInjection;
using OShop.Data.Mapping;

namespace OShop.Presentation.Startup.Ioc
{
    internal class DataAutomapper : ISetup
    {
        public void Execute(IServiceCollection collection)
        {
            collection.AddSingleton(BuildAutomapper());
        }

        private static IAutomapper BuildAutomapper()
        {
            var builder = new AutomapperBuilder();
            return builder.WithMaps().Build();
        }
    }
}
