using Microsoft.Extensions.DependencyInjection;

namespace OShop.Presentation.Startup.Ioc
{
    internal interface ISetup
    {
        void Execute(IServiceCollection collection);
    }
}
