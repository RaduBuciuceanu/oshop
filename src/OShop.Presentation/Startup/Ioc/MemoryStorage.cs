using Microsoft.Extensions.DependencyInjection;
using OShop.Data;

namespace OShop.Presentation.Startup.Ioc
{
    internal class MemoryStorage : ISetup
    {
        public void Execute(IServiceCollection collection)
        {
            collection.AddSingleton<IStorage, Data.MemoryStorage>();
        }
    }
}
