using System.Collections.Generic;
using System.Reactive.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using OShop.Business.Commands.Articles;
using OShop.Business.Models;

namespace OShop.Presentation.Controllers
{
    [EnableCors("*")]
    public class ArticleController : Controller
    {
        private readonly IGetAllArticles _getAllArticles;

        public ArticleController(IGetAllArticles getAllArticles)
        {
            _getAllArticles = getAllArticles;
        }

        [HttpGet("api/article/all")]
        public IEnumerable<Article> All()
        {
            return _getAllArticles.Execute().Wait();
        }
    }
}
