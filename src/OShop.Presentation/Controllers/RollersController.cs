using System.Reactive.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using OShop.Business.Commands.Rollers;
using OShop.Business.Models;

namespace OShop.Presentation.Controllers
{
    [EnableCors("*")]
    public class RollersController : Controller
    {
        private readonly ICreateRollers _createRollers;

        public RollersController(ICreateRollers rollers)
        {
            _createRollers = rollers;
        }

        [HttpPost("api/rollers/create")]
        public Rollers Create([FromBody] Rollers rollers)
        {
            return _createRollers.Execute(rollers).Wait();
        }
    }
}
