using System.Reactive.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using OShop.Business.Commands.Balls;
using OShop.Business.Models;

namespace OShop.Presentation.Controllers
{
    [EnableCors("*")]
    public class BallController : Controller
    {
        private readonly ICreateBall _createBall;

        public BallController(ICreateBall ball)
        {
            _createBall = ball;
        }

        [HttpPost("/api/ball/create")]
        public Ball Create([FromBody] Ball ball)
        {
            return _createBall.Execute(ball).Wait();
        }
    }
}
