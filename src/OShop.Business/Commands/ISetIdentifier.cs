using System;
using OShop.Business.Models;

namespace OShop.Business.Commands
{
    public interface ISetIdentifier
    {
        IObservable<Model> Execute(Model model);
    }
}