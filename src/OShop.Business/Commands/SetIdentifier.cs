using System;
using System.Reactive.Linq;
using OShop.Business.Models;

namespace OShop.Business.Commands
{
    public class SetIdentifier : ISetIdentifier
    {
        public IObservable<Model> Execute(Model model)
        {
            return Observable.Return(Guid.NewGuid())
                .Do(guid => model.Id = guid.ToString())
                .Select(_ => model);
        }
    }
}