using System;
using System.Reactive.Linq;
using OShop.Business.Models;
using OShop.Business.Repositories;

namespace OShop.Business.Commands.Balls
{
    public class CreateBall : ICreateBall
    {
        private readonly IValidateBall _validateBall;
        private readonly ISetIdentifier _setIdentifier;
        private readonly IBallRepository _ballRepository;

        public CreateBall(IBallRepository ballRepository, IValidateBall validateBall, ISetIdentifier setIdentifier)
        {
            _ballRepository = ballRepository;
            _validateBall = validateBall;
            _setIdentifier = setIdentifier;
        }

        public IObservable<Ball> Execute(Ball ball)
        {
            return _validateBall.Execute(ball)
                .Do(validatedBall => _setIdentifier.Execute(validatedBall).Wait())
                .Select(_ballRepository.Insert)
                .Switch();
        }
    }
}