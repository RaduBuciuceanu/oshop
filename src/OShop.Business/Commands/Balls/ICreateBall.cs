using System;
using OShop.Business.Models;

namespace OShop.Business.Commands.Balls
{
    public interface ICreateBall
    {
        IObservable<Ball> Execute(Ball ball);
    }
}