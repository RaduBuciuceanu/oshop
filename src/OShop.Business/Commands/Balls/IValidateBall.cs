using System;
using OShop.Business.Models;

namespace OShop.Business.Commands.Balls
{
    public interface IValidateBall
    {
        IObservable<Ball> Execute(Ball ball);
    }
}