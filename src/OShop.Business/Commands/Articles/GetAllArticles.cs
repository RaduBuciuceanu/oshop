using System;
using System.Collections.Generic;
using OShop.Business.Models;
using OShop.Business.Repositories;

namespace OShop.Business.Commands.Articles
{
    public class GetAllArticles : IGetAllArticles
    {
        private readonly IArticleRepository _articleRepository;

        public GetAllArticles(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IObservable<IEnumerable<Article>> Execute()
        {
            return _articleRepository.GetAll();
        }
    }
}
