using System;
using System.Collections.Generic;
using OShop.Business.Models;

namespace OShop.Business.Commands.Articles
{
    public interface IGetAllArticles
    {
        IObservable<IEnumerable<Article>> Execute();
    }
}
