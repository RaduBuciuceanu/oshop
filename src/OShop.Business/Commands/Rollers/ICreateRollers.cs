using System;
using RollersModel = OShop.Business.Models.Rollers;

namespace OShop.Business.Commands.Rollers
{
    public interface ICreateRollers
    {
        IObservable<RollersModel> Execute(RollersModel rollers);
    }
}
