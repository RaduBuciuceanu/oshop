using System;
using System.Reactive.Linq;
using OShop.Business.Repositories;
using RollersModel = OShop.Business.Models.Rollers;

namespace OShop.Business.Commands.Rollers
{
    public class CreateRollers : ICreateRollers
    {
        private readonly IValidateRollers _validateRollers;
        private readonly ISetIdentifier _setIdentifier;
        private readonly IRollersRepository _rollersRepository;

        public CreateRollers(IRollersRepository rollersRepository, IValidateRollers validateRollers,
            ISetIdentifier setIdentifier)
        {
            _rollersRepository = rollersRepository;
            _validateRollers = validateRollers;
            _setIdentifier = setIdentifier;
        }

        public IObservable<RollersModel> Execute(RollersModel rollers)
        {
            return _validateRollers.Execute(rollers)
                .Do(validatedRollers => _setIdentifier.Execute(validatedRollers).Wait())
                .Select(_rollersRepository.Insert)
                .Switch();
        }
    }
}
