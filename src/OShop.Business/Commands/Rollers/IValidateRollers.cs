using System;
using RollersModel = OShop.Business.Models.Rollers;

namespace OShop.Business.Commands.Rollers
{
    public interface IValidateRollers
    {
        IObservable<RollersModel> Execute(RollersModel rollers);
    }
}