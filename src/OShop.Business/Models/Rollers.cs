namespace OShop.Business.Models
{
    public class Rollers : Article
    {
        public int WheelCount { get; set; }

        public string Size { get; set; }
    }
}
