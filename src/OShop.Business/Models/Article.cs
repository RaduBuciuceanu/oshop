namespace OShop.Business.Models
{
    public class Article : Model
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Price { get; set; }
    }
}
