using System;
using OShop.Business.Models;

namespace OShop.Business.Repositories
{
    public interface IBallRepository
    {
        IObservable<Ball> Insert(Ball ball);
    }
}
