using System;
using System.Collections.Generic;
using OShop.Business.Models;

namespace OShop.Business.Repositories
{
    public interface IArticleRepository
    {
        IObservable<IEnumerable<Article>> GetAll();
    }
}
