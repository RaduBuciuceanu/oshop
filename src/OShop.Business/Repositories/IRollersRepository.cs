using System;
using OShop.Business.Models;

namespace OShop.Business.Repositories
{
    public interface IRollersRepository
    {
        IObservable<Rollers> Insert(Rollers rollers);
    }
}
