using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using OShop.Business.Repositories;
using OShop.Data.Mapping;
using ArticleEntity = OShop.Data.Entities.Article;
using ArticleModel = OShop.Business.Models.Article;

namespace OShop.Data.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly IStorage _storage;
        private readonly IAutomapper _automapper;

        public ArticleRepository(IStorage storage, IAutomapper automapper)
        {
            _storage = storage;
            _automapper = automapper;
        }

        public IObservable<IEnumerable<ArticleModel>> GetAll()
        {
            return _storage.Get<ArticleEntity>()
                .Select(_automapper.MapMany<ArticleEntity, ArticleModel>)
                .Switch();
        }
    }
}