using System;
using System.Reactive.Linq;
using OShop.Business.Repositories;
using OShop.Data.Mapping;
using BallModel = OShop.Business.Models.Ball;
using BallEntity = OShop.Data.Entities.Ball;

namespace OShop.Data.Repositories
{
    public class BallRepository : IBallRepository
    {
        private readonly IStorage _storage;
        private readonly IAutomapper _automapper;

        public BallRepository(IStorage storage, IAutomapper automapper)
        {
            _storage = storage;
            _automapper = automapper;
        }

        public IObservable<BallModel> Insert(BallModel ball)
        {
            return _automapper.Map<BallModel, BallEntity>(ball)
                .Select(_storage.Insert)
                .Switch()
                .Select(_automapper.Map<BallEntity, BallModel>)
                .Switch();
        }
    }
}
