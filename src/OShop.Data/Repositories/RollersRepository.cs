using System;
using System.Reactive.Linq;
using OShop.Business.Models;
using OShop.Business.Repositories;
using OShop.Data.Mapping;
using RollersModel = OShop.Business.Models.Rollers;
using RollersEntity = OShop.Data.Entities.Rollers;

namespace OShop.Data.Repositories
{
    public class RollersRepository : IRollersRepository
    {
        private readonly IStorage _storage;
        private readonly IAutomapper _automapper;

        public RollersRepository(IStorage storage, IAutomapper automapper)
        {
            _storage = storage;
            _automapper = automapper;
        }

        public IObservable<Rollers> Insert(Rollers rollers)
        {
            return _automapper.Map<RollersModel, RollersEntity>(rollers)
                .Select(_storage.Insert)
                .Switch()
                .Select(_automapper.Map<RollersEntity, RollersModel>)
                .Switch();
        }
    }
}