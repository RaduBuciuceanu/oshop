using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using OShop.Data.Entities;

namespace OShop.Data
{
    public class MemoryStorage : IStorage
    {
        private readonly ICollection<Entity> _entities;

        public MemoryStorage()
        {
            _entities = new List<Entity>();
        }

        public IObservable<TEntity> Insert<TEntity>(TEntity entity) where TEntity : Entity
        {
            return EnsureEntityDoesNotExist(entity).Do(_ => _entities.Add(entity));
        }

        public IObservable<TEntity> Update<TEntity>(TEntity entity) where TEntity : Entity
        {
            return Observable.Return(_entities.Single(existent => existent.Id == entity.Id))
                .Select(Delete)
                .Switch()
                .Select(_ => Insert(entity))
                .Switch();
        }

        public IObservable<TEntity> Delete<TEntity>(TEntity entity) where TEntity : Entity
        {
            return Observable.Return(_entities.Single(existent => existent.Id == entity.Id))
                .Do(foundEntity => _entities.Remove(foundEntity))
                .Select(removedEntity => (TEntity)removedEntity);
        }

        public IObservable<IQueryable<TEntity>> Get<TEntity>() where TEntity : Entity
        {
            return Observable.Return(_entities.OfType<TEntity>().AsQueryable());
        }

        private IObservable<TEntity> EnsureEntityDoesNotExist<TEntity>(TEntity entity) where TEntity : Entity
        {
            return Observable.Return(_entities)
                .Select(entities => entities.Any(existent => existent.Id == entity.Id))
                .Select(alreadyExists => alreadyExists ? throw new ArgumentException() : false)
                .Select(_ => entity);
        }
    }
}