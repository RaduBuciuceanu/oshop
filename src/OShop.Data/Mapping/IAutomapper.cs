using System;
using System.Collections.Generic;

namespace OShop.Data.Mapping
{
    public interface IAutomapper
    {
        IObservable<TOutput> Map<TInput, TOutput>(TInput value);

        IObservable<IEnumerable<TOutput>> MapMany<TInput, TOutput>(IEnumerable<TInput> value);
    }
}