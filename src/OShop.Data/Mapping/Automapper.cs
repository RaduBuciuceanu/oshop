using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using AutoMapper;

namespace OShop.Data.Mapping
{
    public class Automapper : IAutomapper
    {
        private readonly IMapper _mapper;

        public Automapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IObservable<TOutput> Map<TInput, TOutput>(TInput value)
        {
            return Observable.Return(_mapper.Map<TInput, TOutput>(value));
        }

        public IObservable<IEnumerable<TOutput>> MapMany<TInput, TOutput>(IEnumerable<TInput> value)
        {
            return Observable.Return(_mapper.Map<IEnumerable<TInput>, IEnumerable<TOutput>>(value));
        }
    }
}
