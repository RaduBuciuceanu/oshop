using AutoMapper;
using ArticleModel = OShop.Business.Models.Article;
using ArticleEntity = OShop.Data.Entities.Article;

namespace OShop.Data.Mapping.Maps
{
    internal class ArticleArticle : IMapping
    {
        public void Execute(IProfileExpression input)
        {
            input.CreateMap<ArticleModel, ArticleEntity>().ReverseMap();
        }
    }
}
