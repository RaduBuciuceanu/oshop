using AutoMapper;
using ArticleModel = OShop.Business.Models.Article;
using ArticleEntity = OShop.Data.Entities.Article;
using RollersModel = OShop.Business.Models.Rollers;
using RollersEntity = OShop.Data.Entities.Rollers;

namespace OShop.Data.Mapping.Maps
{
    public class RollersRollers : IMapping
    {
        public void Execute(IProfileExpression input)
        {
            input.CreateMap<RollersModel, RollersEntity>()
                .IncludeBase<ArticleModel, ArticleEntity>()
                .ReverseMap();
        }
    }
}