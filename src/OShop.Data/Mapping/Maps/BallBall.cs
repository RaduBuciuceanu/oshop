using AutoMapper;
using ArticleModel = OShop.Business.Models.Article;
using ArticleEntity = OShop.Data.Entities.Article;
using BallModel = OShop.Business.Models.Ball;
using BallEntity = OShop.Data.Entities.Ball;

namespace OShop.Data.Mapping.Maps
{
    internal class BallBall : IMapping
    {
        public void Execute(IProfileExpression input)
        {
            input.CreateMap<BallModel, BallEntity>()
                .IncludeBase<ArticleModel, ArticleEntity>()
                .ReverseMap();
        }
    }
}
