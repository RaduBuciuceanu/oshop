using AutoMapper;

namespace OShop.Data.Mapping
{
    public interface IMapping
    {
        void Execute(IProfileExpression input);
    }
}