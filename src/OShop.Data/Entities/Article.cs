namespace OShop.Data.Entities
{
    public class Article : Entity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Price { get; set; }
    }
}