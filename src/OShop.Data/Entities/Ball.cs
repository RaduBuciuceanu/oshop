namespace OShop.Data.Entities
{
    public class Ball : Article
    {
        public string MaximumPressure { get; set; }

        public string Diameter { get; set; }
    }
}
