namespace OShop.Data.Entities
{
    public class Rollers : Article
    {
        public int WheelCount { get; set; }

        public string Size { get; set; }
    }
}
