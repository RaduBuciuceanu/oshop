namespace OShop.Data.Entities
{
    public abstract class Entity
    {
        public string Id { get; set; }
    }
}