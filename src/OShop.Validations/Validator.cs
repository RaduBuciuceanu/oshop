using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using FluentValidation;
using FluentValidation.Results;

namespace OShop.Validations
{
    public abstract class Validator<TModel> : AbstractValidator<TModel>
    {
        public virtual IObservable<TModel> Execute(TModel input)
        {
            return Observable.Return(input)
                .Do(_ => DefineRules())
                .Select(Validate)
                .Do(HandleFoundFailures)
                .Select(_ => input);
        }

        protected abstract void DefineRules();

        private static void HandleFoundFailures(ValidationResult validationResult)
        {
            if (!validationResult.IsValid)
            {
                IEnumerable<string> messages = validationResult.Errors
                    .Select(failure => $"{failure.PropertyName}: {failure.ErrorMessage}");

                throw new ArgumentException(string.Join(Environment.NewLine, messages));
            }
        }
    }
}
