using FluentValidation;
using OShop.Business.Commands.Rollers;
using RollersModel = OShop.Business.Models.Rollers;

namespace OShop.Validations.Rollers
{
    public class ValidateRollers : Validator<RollersModel>, IValidateRollers
    {
        protected override void DefineRules()
        {
            RuleFor(model => model.Id).Empty().WithMessage("Must be null.");
            RuleFor(model => model.Name).NotEmpty().WithMessage("Must not be null or whitespace.");
            RuleFor(model => model.Description).NotEmpty().WithMessage("Must not be null or whitespace.");
            RuleFor(model => model.Price).NotEmpty().WithMessage("Must not be null or whitespace.");
            RuleFor(model => model.WheelCount).NotEmpty().WithMessage("Must not be null or whitespace.");
            RuleFor(model => model.Size).NotEmpty().WithMessage("Must not be null or whitespace.");
        }
    }
}