using FluentValidation;
using OShop.Business.Commands.Balls;
using OShop.Business.Models;

namespace OShop.Validations.Balls
{
    public class ValidateBall : Validator<Ball>, IValidateBall
    {
        protected override void DefineRules()
        {
            RuleFor(model => model.Id).Empty().WithMessage("Must be null.");
            RuleFor(model => model.Name).NotEmpty().WithMessage("Must not be null or whitespace.");
            RuleFor(model => model.Description).NotEmpty().WithMessage("Must not be null or whitespace.");
            RuleFor(model => model.Price).NotEmpty().WithMessage("Must not be null or whitespace.");
            RuleFor(model => model.MaximumPressure).NotEmpty().WithMessage("Must not be null or whitespace.");
            RuleFor(model => model.Diameter).NotEmpty().WithMessage("Must not be null or whitespace.");
        }
    }
}