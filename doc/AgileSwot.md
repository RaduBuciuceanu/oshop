## Agile SWOT analysis

|  | Strengths |
| --- | --- |
| 1. | Requirements are welcome to change even in late development. |
| 2. | High visibility of the dev process. |
| 3. | Simplicity. |
| 4. | Prioritization of the requirements. |
| 5. | Deadline can be extended. |

|  | Weakness |
| --- | --- |
| 1. | Heavy customer interaction. |
| 2. | Lack of documentation. |
| 3. | Team has no time. |

|  | Opportunities |
| --- | --- |
| 1. | Interactive and incremental methodology. |

|  | Threats |
| --- | --- |
| 1. | Lack of documentation. |
