## Risk log

| Date | Description | Likelihood | Impact | Severity | Cause | Status | Responsible |
|---|---|---|---|---|---|---|---|
| 04.15.2019 | Small custom decisions related to features and implementations. | Low | High | Amber | Deadline is too short. | Open | Radu B. |
| 04.15.2019 | A part of testing could be ignored.  | High | High | Amber | Deadline is too short. | Open | Radu B. |
