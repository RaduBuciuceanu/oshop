## OShop Requirements

The OShop project offers an easy API interface for storing shop 
articles for an online shop.

### Scope
The scope of this document is to offer a common overview between 
business people and developers about all the features the application 
should have. 

### Requirements
This section will describe all the requirements of the OShop 
application.

#### Main features
* The ability to store articles of type: `Ball`.
* The ability to store articles of type: `Rollers`.
* The ability to get all stored articles.

#### Implementation details
* Application should expose an _API_ through _HTTP_ protocol.

#### Maintenance features
* Project should be fully covered by _Unit tests_.
* Project should have 3 environments: _Dev_, _Staging_ and 
_Production_.
    * _Dev_ should be used by development team.
    * _Staging_ should be used by QA team.
    * _Production_ should be used by final users.
* Deploy to _Dev_ should be done automatically after all the 
_Unit tests_ passes.
* Deploy to _Staging_ and _Production_ should be made manually from 
_Gitlab_ (by pressing a simple button).
