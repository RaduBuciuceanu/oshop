## PM SWOT analysis 

### Internal factories

|  | Strengths |
| --- | --- |
| 1. | Planning |
| 2. | Team is familiar with .net. |
| 3. | Good tasks breakdown. |

| | Weaknesses |
| --- | --- |
| 1. | Deadline. |
| 2. | The whole team has 1 more job. |

### External factories

| | Opportunities |
| --- | --- |
| 1. | This project can be continued with new requirements. |
| 2. | A new project can be started after this one is done. |

| | Threads |
| --- | --- |
| 1. | Requirements can change. |
| 2. | Deadline can change. |
